package td2;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;




//@WebServlet("/validate")
public class ValidateUser extends HttpServlet {

	private User testUser=null;
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException,IOException{
		
		res.setContentType("text/html");
		PrintWriter pw= res.getWriter();

	    RequestDispatcher rd = req.getRequestDispatcher("userChoice");
		String doesUserExists="no";
		 testUser =new User(
				String.valueOf(req.getParameter("name")),
				String.valueOf(req.getParameter("sourcename")),
				String.valueOf(req.getParameter("email")),
				String.valueOf(req.getParameter("pws")),
				String.valueOf(req.getParameter("gender"))
				);

		if(doesUserExists(testUser)){
			//doesUserExists="yes";
			pw.println("<html><body>");
			pw.println("The users information has been added previously, do you want to duplicate the record?");
			pw.println("The desired user to be registered is: ");
			CreateUser.showUserInformation(testUser, pw);
			pw.println("<br>");
			pw.println("<form action=\"validate\" method=\"get\">");
			pw.println("<label> Yes</label> <input type=\"radio\" name=\"userChoice\" value=\"yes\" >");
		    pw.println("<label> No</label> <input type=\"radio\" name=\"userChoice\"  value=\"no\">"); 
			pw.println("<input type=\"text\" name=\"name\" value=\""+testUser.getName()+"\" >");
			pw.println("<input type=\"text\" name=\"sourcename\" value=\""+testUser.getFamilyName()+"\" >");
			pw.println("<input type=\"email\" name=\"email\" value=\""+testUser.getEmail()+"\" >");
			pw.println("<input type=\"password\" name=\"pws\" value=\""+testUser.getPassword()+"\" >");
			pw.println("<input type=\"radio\" name=\"gender\"  value=\""+testUser.getGender()+"\" >");
			pw.println("<input type=\"submit\" value\"Sent\">");
			pw.println("</form>");	
			pw.println("</body></html>");
		}
		else {
			if(CreateUser.insertUser(testUser)) {
				pw.println("The registered user is: ");
				CreateUser.showUserInformation(testUser,pw);
			}
		}

	}
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException,IOException{
		PrintWriter pw= res.getWriter();
		testUser =new User(
				String.valueOf(req.getParameter("name")),
				String.valueOf(req.getParameter("sourcename")),
				String.valueOf(req.getParameter("email")),
				String.valueOf(req.getParameter("pws")),
				String.valueOf(req.getParameter("gender"))
				);
		
		if(String.valueOf(req.getParameter("userChoice")).equals("yes")) {
			if(CreateUser.insertUser(testUser)) {
				pw.println("The new  registered user is: ");
				CreateUser.showUserInformation(testUser,pw);
			}
		}
		else {
			RequestDispatcher rd = req.getRequestDispatcher("connection.html");
			rd.forward(req, res);
		}
		
		
	}
	public static boolean doesUserExists(User testedUser) {
		boolean doubledUser=false;
		
		
		for(Map.Entry<Integer, User> user: CreateUser.getUsersTable().entrySet()) {
			if(user.getValue().getName().equals(testedUser.getName()) && user.getValue().getFamilyName().equals(testedUser.getFamilyName()) ) {
				System.out.println("The value has changed");
				doubledUser=true;
			}
		}
		
		return doubledUser;
	}

	public User getTestUser() {
		return testUser;
	}

	public void setTestUser(User testUser) {
		this.testUser = testUser;
	}
}