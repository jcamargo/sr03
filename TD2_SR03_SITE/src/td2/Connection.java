package td2;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class Connection extends HttpServlet {
	private User userRegistered=null;
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException,IOException{
		res.setContentType("text/html");
	    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH-mm-ss");  
		Cookie [] cookiesClients =req.getCookies();
		PrintWriter pw = res.getWriter();
		
		if(cookiesClients!=null) {
			//search
			int position=doesUserHasCookies(cookiesClients,"lastConnection");
				pw.println("Lastest connection date  : "+cookiesClients[position].getValue());
			    LocalDateTime now = LocalDateTime.now(); 
			    cookiesClients[position].setValue(now.toString());
			    res.addCookie(cookiesClients[position]);
			
		}
		else {
		    LocalDateTime now = LocalDateTime.now(); 
		    res.addCookie(new Cookie("lastConnection",now.toString()));
		}

		

		
		if(CreateUser.isUserAuthorized(String.valueOf(req.getParameter("email")), String.valueOf(req.getParameter("psw")))) {
			HttpSession session = req.getSession();
			setUserRegistered(CreateUser.findUserByEmail(req.getParameter("email")));
			session.setAttribute("email", req.getParameter("email"));
			session.setAttribute("role", getUserRegistered().getRole());
			if(getUserRegistered().getRole().equals("admin")) {
				
				pw.println("<!DOCTYPE html>");
				pw.println("<head> <title> </title> </head>");
				pw.println("<body>");
				pw.println("<h1>Hello " + session.getAttribute ("email") + "</h1>");
				pw.println ("<nav > <ul>");
				pw.println(" <li>Connected</li>");
				pw.println("<li><a href=\"index.html\">Cr�er un nouveau utilisateur</a></li>");
				//pw.println(" <li><a href=\"index.html\">Afficher la liste des utilisateurs</a></li>");
				pw.println("</ul>");
				pw.println("</nav>");
				pw.println("</body>");
				pw.println("</html>");
			}
			else {
				pw.println("Please demand to the admin to be registered on the platform, your role is \"other\"");
			}

			
		}
		else {
			pw.println("Esa madre tiene un problema");
			RequestDispatcher rd = req.getRequestDispatcher("desconnection");
			rd.forward(req, res);
		}

	}

	public int doesUserHasCookies(Cookie [] c, String email) {
		int position = -1;
		for(int i=0;i<c.length;i++){ 
			if(email.equals(c[i].getName())) {
				position=i;
			}
			// out.print("Name: "+c[i].getName()+" & Value: "+c[i].getValue());
			}
		return position;
	}
	public User getUserRegistered() {
		return userRegistered;
	}

	public void setUserRegistered(User userRegistered) {
		this.userRegistered = userRegistered;
	}
	
}
