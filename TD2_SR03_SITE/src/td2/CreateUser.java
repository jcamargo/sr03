package td2;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//@WebServlet("createUser")
public class CreateUser extends HttpServlet {
	private static Map<Integer, User> usersTable = new HashMap<Integer, User>();
	private static int counter = 0;

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		PrintWriter pw = res.getWriter();
		User newUser = new User(String.valueOf(req.getParameter("name")),
				String.valueOf(req.getParameter("sourcename")), String.valueOf(req.getParameter("email")),
				String.valueOf(req.getParameter("pws")), String.valueOf(req.getParameter("gender")));

		if (insertUser(newUser)) {
			pw.println("The registered user is: ");
			CreateUser.showUserInformation(newUser, pw);
		} else {
			pw.println("The user was not registered ");
		}

	}

	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

		printAllUsers(res.getWriter());
	}

	public static Map<Integer, User> getUsersTable() {
		return CreateUser.usersTable;
	}

	public static boolean insertUser(User newUser) {
		CreateUser.usersTable.put(counter, newUser);
		CreateUser.counter++;

		return true;
	}

	public static void showUserInformation(User user, PrintWriter pw) {
		pw.println("Name: " + user.getName());
		pw.println("Family Name: " + user.getFamilyName());
		pw.println("Email: " + user.getEmail());
		pw.println("Password: " + user.getPassword());
		pw.println("Gender: " + user.getGender());

	}

	public static User findUserByEmail(String email) {
		User userFound = null;
		for (Map.Entry<Integer, User> user : getUsersTable().entrySet()) {
			if (user.getValue().getEmail().equals(email)) {

				userFound = (User) user.getValue();

			}

		}
		return userFound;
	}

	public static boolean isUserAuthorized(String email, String password) {
		for (Map.Entry<Integer, User> user : getUsersTable().entrySet()) {
			if (user.getValue().getEmail().equals(email) && user.getValue().getPassword().equals(password)) {
				return true;
			}

		}
		return false;
	}

	public static void printAllUsers(PrintWriter pw) {
		for (Map.Entry<Integer, User> user : getUsersTable().entrySet()) {
			pw.println("User " + user.getKey());
			showUserInformation(user.getValue(), pw);

		}
	}
}
